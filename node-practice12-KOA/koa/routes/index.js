const router = require('koa-router')()
const db = require("../db/mysql");

router.get('/', async (ctx, next) => {
  var sqlSelect = "select * from chapter";
  var result = await db.query(sqlSelect,[]);
  console.log(result);
  var chapterList = result;
  await ctx.render('index',{
    chapterList:chapterList
  })

})

router.get('/detail/:id', async (ctx, next) => {
  var blogId = ctx.params.id;
  var selectSql = "select * from chapter where chapterId = ?";
  var result = await db.query(selectSql,[blogId]);
  await ctx.render('detail',{
    chapter:result[0]
  })
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router
