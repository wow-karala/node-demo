const router = require('koa-router')();
const getList = require("../controller/blog")

router.prefix('/getChapterList');

router.get('/',async (ctx,next)=>{
    const list = await getList()
    ctx.body = list;
})


module.exports = router;