const {exec} = require('../db/mysql');
const xss = require('xss');//防止攻击

const getList = async()=>{
    let sql = "select chapterName,publishTimer,author,views,chapterContent from chapter";
    return await exec(sql)
}

module.exports = getList;