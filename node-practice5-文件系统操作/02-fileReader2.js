const http = require("http");
const path = require("path");
const fs = require("fs");

var filePath = process.argv[2];//获取命令行参数
var resdata;
var buf = new Buffer.alloc(1024);

http.createServer((req, res) => {
    res.writeHead(200, { "Content-type": "text/html;charset = utf-8" });
    res.write("<head><meta charset = 'UTF-8'/></head>")
    res.end(resdata);

}).listen(8081);
if (filePath != undefined) {
    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err == null) {
            readFile(filePath);
        } else { console.log(err); }
    })
} else { readFile(__filename);}
function readFile(path) {
    fs.open(path, 'r+', (err, fd) => {//文件名  操作标识：r，读方式打开,回调函数：fd为一个整数，打开文件返回的文件描述符
        if (err) {
            console.error(err);
        }
        console.log(fd);
        fs.read(fd, buf, 0, buf.length, 0, (err, bytes) => {//fd buffer offset:向缓存区中写入的初始位置，length：读取文件的长度；position:读取文件的初始位置;回调函数：实际读取的字节数
            resdata = buf.slice(0, bytes).toString()
        })
        fs.close(fd, (err) => {
            if (err) {
                console.log(err);
            } else { console.log("文件关闭成功") };
        })
    })
}


