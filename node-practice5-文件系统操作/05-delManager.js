const fs = require("fs");
const path = require("path");
var arg = process.argv[2];
var filePath = path.join(__dirname,"/"+arg);


/*function delManager(filePath){
    if(fs.existsSync(filePath)){
        var files = fs.readdir(filePath);
        for(var i = 0;i<files.length;i++){
            var childPath = path.join(filePath,"/"+files[i]);
            var fileObj = fs.statSync(childPath);
            if(fileObj.isFile()){
                fs.readlink(childPath);
            }
            else if(fileObj.isDirectory()){
                delManager(childPath);
            }            
        }
        fs.rmdir(filePath);
    }
    else{
        console.log("路径不存在");
    }
}
delManager(filePath);*/
function delManager(filePath){
    if(fs.existsSync(filePath)){
        var fileObj = fs.statSync(filePath);
        if(fileObj.isFile()){
            fs.unlinkSync(filePath);
        }
        else if(fileObj.isDirectory()){
            var files = fs.readdirSync(filePath);
            for(var i = 0;i<files.length;i++){
                var childPath = path.join(filePath,"/"+files[i]);
                delManager(childPath);
            }
            fs.rmdirSync(filePath); 
        }
    }
    else{
        console.log("路径不存在");
    }
}
delManager(filePath);