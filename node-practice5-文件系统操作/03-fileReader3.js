const http = require("http");
const fs = require("fs");

var filePath = process.argv[2];//获取命令行参数
var resdata;

http.createServer((req, res) => {
    res.writeHead(200, { "Content-type": "text/html;charset = utf-8" });
    res.write("<head><meta charset = 'UTF-8'/></head>")
    res.end(resdata);
}).listen(8081);
if (filePath != undefined) {
    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err == null) {
            readFile(filePath);
        } else { console.log(err); }
    })
} else { readFile(__filename);}
function readFile(path) {
    var read_able = fs.createReadStream(path);
    read_able.on("data",chunk=>{
        resdata = chunk.toString("utf-8");
    })
   
}
/**区别
 * createReadStream是给你一个ReadableStream，你可以听它的'data'，一点一点儿处理文件，用过的部分会被GC（垃圾回收），所以占内存少。
 *  readFile是把整个文件全部读到内存里。 */


