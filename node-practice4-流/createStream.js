var stream = require("stream");
var Readable = stream.Readable;
function MyReadable(){
    Readable.call(this);
}
MyReadable.prototype = Readable.prototype;
var rs = new MyReadable();
var x = 'a'.charCodeAt(0);
rs._read = function(){
    if(x<='z'.charCodeAt(0)){
        rs.push(String.fromCharCode(x++));
    }
    else{
        rs.push(null);
    }
}
rs.pipe(process.stdout);