var http = require("http");
var fs = require("fs");
var path = require("path");
var filePath = path.join(__dirname,"/data.txt");
http.createServer((req,res)=>{
    var readable = fs.createReadStream(filePath);
    readable.pipe(res);

}).listen(8081);