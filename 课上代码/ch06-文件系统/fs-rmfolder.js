const fs = require("fs");
const path = require("path");
var arg = process.argv[2];
var filePath = path.join(__dirname,"/"+arg);
console.log(filePath);

function delDir(filePath){
    if(fs.existsSync(filePath)){
        var files = fs.readdirSync(filePath);
        for(var i = 0;i<files.length;i++){
            var childPath = path.join(filePath,"/"+files[i]);
            var fileObj = fs.statSync(childPath);
            if(fileObj.isFile()){
                fs.unlinkSync(childPath);
            }
            else if(fileObj.isDirectory()){
                delDir(childPath);
            }
        }
        fs.rmdirSync(filePath);
    }else{
        console.log("文件路径不存在");
    }
}
delDir(filePath);