const fs = require("fs");
const path = require("path");

var pathFrom = path.join(__dirname,"/1.txt");
var pathTo = path.join(__dirname,"/8.txt");
var readStream = fs.createReadStream(pathFrom);
var writeStream = fs.createWriteStream(pathTo);
readStream.pipe(writeStream);

writeStream.on("close",()=>{
    var fileObj = fs.statSync(pathFrom);
    //赋予文件权限//
    fs.chmodSync(pathTo,fileObj.mode);
})