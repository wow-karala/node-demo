const fs = require("fs");
const path = require("path");

var filePath = path.join(__dirname,"/2.txt");

var readable  = fs.createReadStream(filePath);
var writeable = fs.createWriteStream(path.join(__dirname,"/4.txt"));
readable.pipe(writeable);