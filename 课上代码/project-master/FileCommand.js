const fs = require("fs");
const path = require("path");

const dirPath = path.join(__dirname,"./sysFile");
const instr = process.argv[2];
var arr = [];
if(instr == '-list'){
    var list = fs.readdirSync(dirPath);
    for(var i = 0; i<list.length;i++){
        var childPath = path.join(dirPath,"/" + list[i]);
        var info  = fs.statSync(childPath);
        var obj = {
            fileName:list[i],
            fileSize:info.size,
            createTime:info.ctimeMs
        }
        arr.push(obj);
    }
    arr.sort(sort);
    arr.map(item=>item.createTime = createTime(item.createTime));
    var arr1 = JSON.stringify(arr);
    console.log(arr1);
}
if(instr == '-show'){
    var list = fs.readdirSync(dirPath);
    for(var i = 0; i<list.length;i++){
        var childPath = path.join(dirPath,'/'+list[i]);
        var flag = 0;
        if(process.argv[3] == list[i]){
            var fileContent = fs.readFileSync(childPath);
            console.log(fileContent.toString());
            flag = 1;
        }
    }
    if(flag == 0){
        console.log('文件不存在')
    }
}
if(instr == '-del'){
    var delName = process.argv[3];
    console.log(delName);
    var delPath = path.join(dirPath,'/'+delName);
    function delFile(delPath){
        if(fs.existsSync(delPath)){
            var obj = fs.statSync(delPath);
            if(obj.isFile()){
                fs.unlinkSync(delPath);
                console.log(delName + '删除成功');
            }
            if(obj.isDirectory(delPath)){
                var files = fs.readdirSync(delPath);
                for(var i=0;i<files.length;i++){
                    var childPath = path.join(delPath,'/'+files[i]);
                    delFile(childPath);
                }
                fs.rmdirSync(delPath);
                console.log(delName + "删除成功");
            }
        }
        else{
            console.log('文件不存在');
        }
    }
    delFile(delPath);
}
function createTime(ctimeMs){
    var ctime = new Date(ctimeMs);
    var year = ctime.getFullYear();
    var month = ctime.getMonth()+1;
    var day = ctime.getDate();
    var hour = ctime.getHours();
    var minute = ctime.getMinutes();
    return `${year}-${month}-${day} ${hour}:${minute>10?minute:'0'+minute}`;
}
function sort(a,b){
    return a.createTime - b.createTime;
}
