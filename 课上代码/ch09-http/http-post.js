const http = require("http");
const url = require("url");

var reqUrl =  "http://localhost:8080/";
var options = url.parse(reqUrl);
options.method = "POST";
var user = {
    userName:"zhangsan",
    age:20,
    sex:"male"
}
var userStr = JSON.stringify(user);

var request = http.request(options,(res)=>{
    res.pipe(process.stdout);
})
request.write(userStr);
request.end();