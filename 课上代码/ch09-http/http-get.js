const http = require("http");
//http.get(url,(res)=>{})
//向特定的url发起http的get请求
//回调函数得到的参数是一个流对象
//可以以流处理的方式来接收其中的数据

http.get("http://nodejs.cn/",(res)=>{
    var str = "";
    res.on("data",(chunk)=>{
        str += chunk;
    })
    res.on("end",()=>{
        console.log(str.toString("utf-8"));
    })
})