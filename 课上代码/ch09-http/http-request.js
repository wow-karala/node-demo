const http = require("http");
const fs = require("fs");
const path = require("path");
const url = require("url");
var reqUrl = "http://nodejs.cn/";
var obj = url.parse(reqUrl);//将url转换成一个对象

var filePath = path.join(__dirname,"./index.html");
var writeStream = fs.createwriteStream(filePath);

var options = {
    hostname:"nodejs.cn",
    port:80,
    path:"/",
    method:"GET"
}
var request = http.request(options,(res)=>{
    var str = "";
    res.on("data",(chunk)=>{
        str += chunk;
        res.pipe(writeStream);//res是一个继承于流
        
    })
    res.on("end",()=>{
        console.log(str.toString("utf-8"));
        fs.writeFile(filePath,str,(err)=>{
            if(err){
                console.log(err);
            }
        })
    })  
})
//http.request方法一定要调用end方法 
request.end();