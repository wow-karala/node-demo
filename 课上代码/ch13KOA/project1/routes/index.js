const fs = require("fs");
const path = require("path");

const router = require('koa-router')()
/**
 * 请求的url的pathname和路由文件中路由方法的第一个参数匹配，并且请求类型也相同时，会执行对应的回调函数
   响应页面，ctx.render(被响应页面的路径，页面上填充的数据)
*/
router.get('/', async (ctx, next) => {
  /*await ctx.render('index', {
    title: 'Hello Koa 2!'  //找到index.ejs，向前端响应
  })*/
  var USerList = [{userName:"zhangsan",age:20},{userName:"lisi",age:20}]
  await ctx.render("index",{userList:USerList});
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }//直接赋值，就可以传送json
})

router.post('/',async c=>{

})

router.get('/test',async (ctx,next)=>{
  console.log(ctx.query);//获取到传递的参数

})
router.get('/show/:id',async (ctx,next)=>{
  console.log(ctx.params);//获取到传递的参数
})

router.post('/login',async (ctx,next)=>{
  console.log(ctx.request.body);//post请求获取数据的方式
  ctx.body = obj;
})

router.post('/file',async (ctx,next)=>{
  const file = ctx.request.files.file;
  ctx.body = file.name + '上传成功';
})

module.exports = router
