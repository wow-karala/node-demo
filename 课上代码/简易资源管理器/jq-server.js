const http = require("http");
const path = require("path");
const fs = require("fs");

http.createServer((req,res)=>{
    res.writeHead(200,{"Content-Type":"text/plain;charset = utf-8"})
    if(req.url == "/"){
        var filePath = path.join(__dirname,"/test.html");
        var fileContent = fs.readFileSync(filePath);
        res.write(fileContent);
        res.end();
    }
    else if(req.url == "/getData"){
        var arr = [{userName:"zhangsan",age:20},{userName:"lisi",age:23}];
        var jsonStr = JSON.stringify(arr);
        //res.writeHead(200,{"Content-Type":"text/plain;charset = utf-8"})
        res.write(jsonStr);
        res.end();
    }
}).listen(8083);
console.log("listening 8083");