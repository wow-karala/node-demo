const http = require("http");
const path = require("path");
const fs = require("fs");

http.createServer((req,res)=>{
    var url = req.url;
    var index = path.join(__dirname,"/index.html");
    var img = path.join(__dirname,"/1.png");
    if(url == "/"){
        var fileContent = fs.readFileSync(index);
        res.writeHead(200,{"Content-Type":"text/html;charset = utf-8"});
        res.write(fileContent);
        res.end();
    }
    if(url == "/1.png"){
        var imgContent = fs.readFileSync(img);
        res.writeHead(200,{"Content-Type":"image/png"});
        res.write(imgContent);
        res.end();
    }
    if(url == "/getlist"){
        var arr = [{fileType:"folder",fileName:"lala",fileSize:"125",fileTime:"2021-1-3"},]
        var filePath = path.join(__dirname,"/fileDir");
        var list = fs.readdirSync(filePath);
        for(var i = 0; i<list.length;i++){
            var childPath = path.join(filePath,"/"+list[i]);
            var fileObj = fs.statSync(childPath);
            console.log(fileObj);
            var ctimeMs= new Date(fileObj.ctimeMs);
            var time = createTime(ctimeMs);
            if(fileObj.isDirectory()){
                var obj = {
                    fileType:"folder",
                    fileName:list[i],
                    fileSize:fileObj.size + "byte",
                    fileTime:time
                }
                arr.push(obj);
            }
            if(fileObj.isFile()){
                var obj = {
                    fileType:"file",
                    fileName:list[i].slice(0,-3),
                    fileSize:fileObj.size + "byte",
                    fileTime:time
                }
                arr.push(obj);
            }

        }
        var jsonStr = JSON.stringify(arr);
        res.end(jsonStr);
    }
}).listen(8081);
function createTime(ctimeMs){
    var ctime = new Date(ctimeMs);
    var year = ctime.getFullYear();
    var month = ctime.getMonth()+1;
    var day = ctime.getDate();
    var hour = ctime.getHours();
    var minute = ctime.getMinutes();
    //var str = ctime.toLocaleString();
    //console.log(ctime.toLocaleString());
    //str.replace();
    return `${year}-${month}-${day} ${hour}:${minute}`;
}