const {fork} = require("child_process");

var childProcess = fork("fork-child.js");
var index = 0;
childProcess.send("this is message from parent process");

var intervalId = setInterval(()=>{
    index ++;
    childProcess.send("this is message from parent process");
    if(index == 10){
        clearInterval(intervalId);
    }
},2000);
childProcess.on("message",(chunk)=>{
    console.log(chunk);
})