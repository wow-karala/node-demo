const {execFile} = require("child_process");

//(可执行程序名称/路径，[参数]，回调函数);
//不能执行shell的指令
execFile("node",["exec-child.js", "test"],(err,stdout)=>{
    if(err){
        console.log(err);
    }
    else{
        console.log(stdout);
    }
})