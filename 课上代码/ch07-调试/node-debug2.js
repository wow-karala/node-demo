const http = require("http");
try{
http.createServer((req,res)=>{
    //var str = "hello world";
    res.end(str);
}).listen(8081);
//var test = "server is listening 8081";
console.log(test);
}catch(err){
    console.log(err);
    console.log(err.constructor.name);
}
//try catch只能捕获到同步代码中的异常
process.on("uncaughtException",(err)=>{
    console.log(err.message);
    console.log(err.constructor.name);
})
//用于全局捕获异常，可以捕获到异步代码中的异常