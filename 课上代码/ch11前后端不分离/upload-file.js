const http = require("http");
const fs = require("fs");

const path = require("path");

http.createServer((req,res)=>{
    if(req.url == "/"){
        var fileContent = fs.readFileSync("./upload.html")
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"});
        res.end(fileContent);

    }
    else{
        var str = "";
        req.setEncoding("binary");
        req.on("data",(chunk)=>{
            str += chunk;
        })
        req.on("end",()=>{
            console.log(str);
            var arr = str.split("\r\n");
            var imgArr = arr.slice(4,arr.length-2);
            var imgStr = imgArr.join("\r\n");
            var buf = Buffer.from(imgStr,"binary");
            fs.writeFileSync(path.join(__dirname,"/img/1.png"),buf,{"encoding":"binary"});
            console.log(buf); 
        })
    }

}).listen(8083);