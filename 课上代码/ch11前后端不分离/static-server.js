const http = require("http");
const fs = require("fs");
const path = require("path");

http.createServer((req,res)=>{
    if(req.url == "/"){
        var filePath = path.join(__dirname,"/rest.html");
        var fileContent = fs.readFileSync(filePath);
        res.writeHead(200,{"content-type":"text/html;charset=utf-8"});
        res.write(fileContent);
        res.end();
    }

}).listen(3000)