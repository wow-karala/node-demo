const http = require("http");
/**
 * 跨域问题，不同的协议、IP、端口下面的页面发起的http请求会出现跨域问题
 */
http.createServer((req,res)=>{
    if(req.url == "/"){
        var str = "";
        req.on("data",(chunk)=>{
            str += chunk;
        })
        req.on("end",()=>{
            console.log(str);
            var arr = str.split("\r\n");
            var index = arr.indexOf("");
            var index2 = arr.indexOf("",index+1);
            var obj = {"username":arr[index+1],age:arr[index2+1]};
            console.log(arr);
            console.log(obj);
            res.setHeader("Access-Control-Allow-Origin","*");//允许跨域
            res.writeHead(200,{"content-type":"text/plain"});
            res.end("received");
        })
    }
}).listen(8000);
console.log("server is listening 8000");