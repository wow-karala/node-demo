var stdin = process.stdin;//接收用户的输入，继承可读流的构造函数

/* stdin继承了EventEmitter
继承了stream.Readable
 */

stdin.on("data",function(chunk){
    var chunk = chunk.toString("utf-8");
    var result = chunk.slice(0,-2);
    if(result == ""){
        stdin.emit("end");
    }
});
stdin.on("end",function(){
    console.log("stdin end");
})