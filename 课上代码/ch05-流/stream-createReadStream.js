var fs = require("fs");
var path = require("path");

var filePath = path.join(__dirname,"/1.txt");
var readable = fs.createReadStream(filePath);
readable.on("data",function(chunk){
    console.log(chunk.toString("utf-8"));

})
readable.on("end",function(){
    console.log("readable ended");
})
readable.pipe(process.stdout);