var events = require("events");
var Emitter = events.EventEmitter;

function Dog(name,energy){
    Emitter.call(this);
    this.name = name;
    this.energy = energy;
    var intervalId = setInterval(()=>{
        if(this.energy>=0){
            this.emit("bark");
            this.energy--;
        }
        else{
            clearInterval(intervalId);
        }
    },1000)
}
/*  */

/*for(var i in Emitter.prototype){
    Dog.prototype[i] = Emitter.prototype[i];
}*/
Dog.prototype = Emitter.prototype;
module.exports = Dog;