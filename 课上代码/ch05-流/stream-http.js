var http = require("http");
var path = require("path");
var fs = require("fs");

http.createServer((req,res)=>{
    var filePath = path.join(__dirname,"/2.txt");
    var readable = fs.createReadStream(filePath);
    readable.pipe(res);
}).listen(8081)

console.log("server is listening 8081");