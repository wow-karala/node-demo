var stream = require("stream");
var Readable = stream.Readable;
var rs = new Readable();
var c = 'a'.charCodeAt(0);//uni-code码
rs._read = function(){
    if(c<'z'.charCodeAt(0)){
        rs.push(String.fromCharCode(c++));
    }
    else{
        rs.push(null);
    } 
}
rs.pipe(process.stdout);//将一个可读流输出为一个可写流