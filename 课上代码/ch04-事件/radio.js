var events = require("events");
var util = require("util");
var Emitter = events.EventEmitter;
function Radio(){
    Emitter.call(this);
}
//Radio.prototype = Emitter.prototype;//构造函数的继承
util.inherits(Radio,Emitter);
Radio.prototype.start = function(){
    console.log("music start");
}
Radio.prototype.stop = function(){
    console.log("music stop");
}
module.exports = Radio;