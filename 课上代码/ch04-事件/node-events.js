var events = require("events");//原生模块
var Emitter = events.EventEmitter;//构造函数
var eventObj = new Emitter();//实例化构造函数
eventObj.on("sayhello",function(){//事件监听
    console.log("hello world");
})
setTimeout(() => {//触发事件
    eventObj.emit("sayhello")   
}, 3000);