var events = require("events");
var Emitter = events.EventEmitter;
function Dog(dogName){
    Emitter.call(this);//继承EventEmitter
    this.dogName =dogName;

}
Dog.prototype = Emitter.prototype;//继承EventEmitter
Dog.prototype.bark = function(){
    console.log(this.dogName + "barked");
}
module.exports = Dog;//对外公布Dog构造函数