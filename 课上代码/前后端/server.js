const http = require("http");
const fs = require("fs");
const path = require("path");

http.createServer((req,res)=>{
    console.log(req.url);
    var urlStr = req.url;
    var filePath = path.join(__dirname,"/test.html");
    var imgPath = path.join(__dirname,"/icon.png");
    if(urlStr == "/"){
        var fileContent = fs.readFileSync(filePath);
        res.writeHead(200,{"Content-Type":"text/html;charset = utf-8"});
        res.write(fileContent);
        res.end();
        //res.end(fileContent);
    }
    else if(urlStr == "/icon.png"){
        var imgContent = fs.readFileSync(imgPath);
        res.writeHead(200,{"Content-Type":"image/png"});
        res.write(imgContent);
        res.end(); 

    }
}).listen(8082);
console.log("server is listening 8082");