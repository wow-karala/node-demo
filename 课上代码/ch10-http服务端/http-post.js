const http = require("http");
const fs = require("fs");

var userList = [];

function getDta(str){
    var arr = str.split("&");
    var obj = {};
    for(var i = 0;i<arr.length;i++){
        var childArr = arr[i].split("=");
        obj[childArr[0]] = childArr[1];
    }
    return obj;
}
http.createServer((req,res)=>{
    var urlObj = new URL(req.url,"http://localhost:8081/");
    var pathname = urlObj.pathname;
    if(pathname == "/"){
        var fileContent = fs.readFileSync("./post.html");
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"})
        res.write(fileContent);
        res.end();
    }
    else if(pathname == "/list"){
        var fileContent = fs.readFileSync("./list.html");
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"})
        res.write(fileContent);
        res.end();

    }
    else if(pathname == "/save"){
        console.log(req.headers);
        var str = ""
        req.on("data",(chunk)=>{
            str += chunk;  //转换成字符串进行拼接
        })
        req.on("end",()=>{
            console.log(str);
            var postObj = getDta(str);
            userList.push(postObj);
            res.end("submit success");
        })
    }
    else if(pathname == "/getlist"){
        var userStr =JSON.stringify(userList);
        res.writeHead(200,{"content-type":"text/plain"});
        res.end(userStr);
    }
    else if(pathname == '/del'){
        var delUser = urlObj.searchParams.get("deluser");//得到delUser
        /*userList = userList.filter(({username,age,choose})=>username!= delUser)*/
        for(var i=0;i<userList.length;i++){
            if(userList[i]["username"] == delUser){
                userList.splice(i,1);
                res.writeHead(200,{"content-type":"text/plain"});
                res.end('delete success');
                return;
            }
        }
    }
}).listen(8081);