var exitCode = process.argv[2];
if(typeof(exitCode) == 'undefined' || isNaN(Number(exitCode))){
    console.log("命令行参数不正确");
    process.exit();
}
else{
    console.log("退出码：" + exitCode);
    process.exit(exitCode);
}