var http = require("http");
var fs = require("fs");
var path = require("path");
http.createServer((req,res)=>{
    //res.end("hello world");
    var filePath = path.join(__dirname,"/picture.jpg");
    var imgBuf = fs.readFileSync(filePath);
    //console.log(imgBuf);
    var imgBase64 = imgBuf.toString("base64");
    //console.log(imgBase64);
    res.writeHead(200,{"Content-Type": "text/html"});
    res.write("<!DOCTYPE html></html>" + "<head></head><body>" + "<img src = 'data:image/jpg; base64, "+ imgBase64 +"'>" + "</body></html>");
    res.end();
}).listen(8081);

console.log("server is listening");