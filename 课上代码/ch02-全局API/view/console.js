
var log = console.log;
var user = {username:"zhangsan",age:20,sex:"male"};
log("username:%s",user.username);
log("age:%d",user.age);
log("user:%j",user);
log(`${user.username}`);


log('architecture:',process.arch);//编译 Node.js 二进制文件的操作系统 CPU 架构
log('plateform:%s\n',process.platform);//返回标识运行 Node.js 进程的操作系统平台的字符串。

log('process id:',process.pid);//属性返回进程的 PID。
log('exePath:%s\n',process.execPath);//属性返回启动 Node.js 进程的可执行文件的绝对路径名。 符号链接（如果有）会被解析。

log('node version:',process.version);
//log('user id:',process.getuid());
//log('group id:',process.getgid());
log('cwd:%s\n',process.cwd());//返回 Node.js 进程的当前工作目录。

log('rss:',process.memoryUsage().rss);//进程的内存使用量（以字节为单位）的对象。
log('heapTotal:',process.memoryUsage().heapTotal);
log('heapUsed:',process.memoryUsage().heapUsed);
log('external:%s\n',process.memoryUsage().external);

log('env:',process.env);//属性返回包含用户环境的对象。
log('host name:',process.env.HOSTName);

