var module = require("./my-module");

module.sayHello();//直接把对象赋值，在其他模块的引用过程中，可能会覆盖对象上的属性方法

var module1 = require("./module2");
const { sayHello } = require("./my-module");
var obj = module1();
obj.sayHello();//通过函数赋值，每一次的函数调用过程会生成一个新的对象，可以使用该对象完成对模块方法的调用