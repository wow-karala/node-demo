const http = require("http");
const fs = require("fs");
const path = require("path");
var imgList = [];

http.createServer((req,res)=>{
    console.log(req.url);
    var urlObj = new URL(req.url,"http://localhost:8081/");
    console.log(urlObj);
    var pathname = urlObj.pathname;
    if(req.url == "/"){
        var fileContent = fs.readFileSync(path.join(__dirname,"/index.html"));
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"});
        res.end(fileContent);
    }
    if(pathname == "/upload"){
        var str = "";
        req.setEncoding("binary");
        req.on("data",(chunk)=>{
            str += chunk;
        })
        req.on("end",()=>{
            //console.log(str);
            var arr = str.split("\r\n");
            //console.log(arr);
            var name = arr.slice(1,arr.length-5);
            var imgArr = arr.slice(4,arr.length-2);
            var filename = (name[0].split(";"))[2];
            var imgname= filename.slice(11,-1);
            console.log(imgname);
            var imgStr = imgArr.join("\r\n");
            var buf = Buffer.from(imgStr,"binary");
            fs.writeFileSync(path.join(__dirname,`/upload/${imgname}`),buf,{"encoding":"binary"});
            imgList.push(imgname);
        })
    }
    if(pathname == '/getlist'){
        var userStr =JSON.stringify(imgList);
        res.writeHead(200,{"content-type":"text/plain"});
        res.end(userStr);
    }
    if(pathname == "/list"){
        var fileContent = fs.readFileSync(path.join(__dirname,"/list.html"));
        console.log(imgList);
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"});
        res.end(fileContent);
    }
    if(pathname.indexOf('/upload/') != -1){
        var arr = pathname.split('/');
        var imgName = arr[2];
        var filePath = path.join(__dirname,`/upload/${imgName}`);
        var imgBuf = fs.readFileSync(filePath);
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write(imgBuf);
        res.end();
    }
    /*if(pathname == "/getlist/upload"){
        var delUser = urlObj.searchParams.get("");
    }*/
}).listen(8083)