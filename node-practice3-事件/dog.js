var events = require("events");
var Emitter = events.EventEmitter;
function Dog(name,energy){
    Emitter.call(this);
    this.name = name;
    this.energy = energy;
}
Dog.prototype = Emitter.prototype;

Dog.prototype.bark = function(){
    var intervalId = setInterval(()=>{
        this.energy >0 && this.energy--;
        console.log(this.name + " barked!" + "energy:" + this.energy);
        if(this.energy == 0){
            clearInterval(intervalId);
            console.log(this.name + "能量耗尽");
        }
    },1000);  
}
module.exports = Dog;
