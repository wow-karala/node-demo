const http = require("http");
const mysql = require("mysql");
const fs = require("fs");
const path = require("path");

var con = mysql.createConnection({
    host:"192.168.23.144",
    port:3306,
    user:"root",
    password:"ddd",
    database:"mydatabase"
})

http.createServer((req,res)=>{
    var urlObj = new URL(req.url,"http://localhost:8084/");
    var pathname = urlObj.pathname;
    if(pathname == "/"){
        var filePath = path.join(__dirname,"/chapterList.html")
        var fileContent = fs.readFileSync(filePath);
        res.writeHead(200,{"content-type":"text/html;charset = utf-8"});
        res.end(fileContent);
    }
    if(pathname == "/getDetail"){
        //var chapterId = urlObj.searchParams.get("chapterId");
        var info = "select chapterName,publishTimer,author,views,chapterContent from chapter";
        con.query(info,[],(err,result)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log(result);
                res.writeHead(200,{"content-type":"text/json"});
                res.end(JSON.stringify(result));
            }
        })

    }

}).listen(8084);