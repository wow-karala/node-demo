function circumference(r){
    var circumference = Math.PI * 2 *r;
    return circumference;
}
function area(r){
    var area = Math.PI * r * r;
    return area;
}
var circle = {
    circumference:circumference,
    area:area
}
module.exports = circle;