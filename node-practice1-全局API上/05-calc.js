var arg = process.argv[2];//返回数组，第一个元素是node.js可执行路径，第二个元素是正在执行的js路径，然后是命令行参数;
var result = eval(arg);
if(typeof(arg) == 'undefined'){
    console.log("请输入指令");
}
else if(arg == '--help' || arg =='-h'){
    console.log('程序的使用说明');
}
else if(!isNaN(result)){
    console.log(`${arg}=${result}`);
}
else{
    console.log("表达式不合法");
}